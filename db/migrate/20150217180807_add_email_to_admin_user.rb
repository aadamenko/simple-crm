class AddEmailToAdminUser < ActiveRecord::Migration
  def up
  	add_column "admin_users", "email", :string
  end
  def down
  	remove_column "admin_users", "email"
  end
end
