class PublicController < ApplicationController
  before_action :setup_navigation

  layout 'public'

  def index
  	#intro text
  end

  def show
  	@page = Page.where( :permalink => params[:permalink], :visible => true ).first
  	if @page.nil?
  		redirect_to( :action => 'index' )
  	else
  		#display page on show.html.erb
  	end
  end

  def setup_navigation
  	@subjects = Subject.visible.sorted
  	
  end
end
