class Section < ActiveRecord::Base
	belongs_to :page
	has_many :section_edits
	has_many :editors, :through => :section_edits, :class_name => "AdminUser"

	after_save :touch_page

	acts_as_list :scope => :page
	CONTENT_TYPES = ['text', 'HTML']
	validates_presence_of :name

	validates_length_of :name, :maximum => 255

	validates_inclusion_of :content_type, :in => ['text', 'HTML'],
	:message => "Must be one of: #{CONTENT_TYPES.join(', ')}"

	scope :visible, lambda { where(:visible => true) }
	scope :invisible, lambda { where(:visible => false) }
	scope :sorted, lambda { order("sections.position ASC") }
	scope :newest_first, lambda { order("sections.created_at DESC") }
	scope :search, lambda {|name| 
		where(["name LIKE ?", "%#{name}%"]) 
	}

	private
	def touch_page
		page.touch
		
	end
end
