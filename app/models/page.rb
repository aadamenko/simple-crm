class Page < ActiveRecord::Base
	belongs_to :subject
	has_many :sections
	acts_as_list :scope => :subject
	#has_and_belongs_to_many :admin_users#, :join_table => "admins_users_pages"
	has_and_belongs_to_many :editors, :class_name => "AdminUser"

	before_validation :add_default_permalink
	after_save :touch_subject
	after_destroy :delete_related_sections

	validates_presence_of :name

	validates_length_of :name, :maximum => 255

	validates_presence_of :permalink

	#validates_length_of :permalink, :within => 3..255

	validates_uniqueness_of :permalink

	scope :visible, lambda { where(:visible => true) }
	scope :invisible, lambda { where(:visible => false) }
	scope :sorted, lambda { order("pages.position ASC") }
	scope :newest_first, lambda { order("pages.created_at DESC") }
	scope :search, lambda {|name| 
		where(["name LIKE ?", "%#{name}%"]) 
	}

	private 
	def add_default_permalink
		if permalink.blank?
			self.permalink = 0
		end
	end

	def touch_subject
		subject.touch
		
	end

	def delete_related_sections
		self.section.each do |s|
			#s.destroy
		end
	end
end
